## GitLab's functional group updates

We renamed [functional group updates to group conversations][fgu-gc].
This project hence moved to [gitlab-org/group-conversations].

[fgu-gc]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/15416
[gitlab-org/group-conversations]: https://gitlab.com/gitlab-org/group-conversations
